---
output: 
  html_document: 
    keep_md: yes
---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# fonds_carto_Metro_DOM

# Contenu du dépôt

Ce projet contient les fonds de carte en géographie 2020 pour les mailles régionales et départementales (avec zoom sur la petite couronne parisienne).

**Arborescence du dépôt**  

```
▒   .gitignore
▒   README.Rmd
▒   README.md
▒
▒▒▒▒fonds_detailles
        dep_France_DOM_zoomPC.cpg
        dep_France_DOM_zoomPC.dbf
        dep_France_DOM_zoomPC.prj
        dep_France_DOM_zoomPC.qpj
        dep_France_DOM_zoomPC.shp
        dep_France_DOM_zoomPC.shx
        reg_France_DOM.cpg
        reg_France_DOM.dbf
        reg_France_DOM.prj
        reg_France_DOM.qpj
        reg_France_DOM.shp
        reg_France_DOM.shx
```

## Exemples d'utilisation 


```{r message=FALSE, warning=FALSE}
library(sf)
library(ggplot2)
```

Lecture des fonds de carte avec {sf} :

```{r}
fonds_dept <- st_read("fonds_detailles/dep_France_DOM_zoomPC.shp")
fonds_reg <- st_read("fonds_detailles/reg_France_DOM.shp")
```

Affichage du fonds de carte départemental :  

```{r}
ggplot(fonds_dept) +
  geom_sf() +
  aes(fill = reg) +
  coord_sf(crs = 4326) +
  guides(fill = FALSE) +
  ggtitle("Ma carte par département")
```

