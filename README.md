---
output: 
  html_document: 
    keep_md: yes
---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->



# fonds_carto_Metro_DOM

# Contenu du dépôt

Ce projet contient les fonds de carte en géographie 2020 pour les mailles régionales et départementales (avec zoom sur la petite couronne parisienne).

**Arborescence du dépôt**  

```
▒   .gitignore
▒   README.Rmd
▒   README.md
▒
▒▒▒▒fonds_detailles
        dep_France_DOM_zoomPC.cpg
        dep_France_DOM_zoomPC.dbf
        dep_France_DOM_zoomPC.prj
        dep_France_DOM_zoomPC.qpj
        dep_France_DOM_zoomPC.shp
        dep_France_DOM_zoomPC.shx
        reg_France_DOM.cpg
        reg_France_DOM.dbf
        reg_France_DOM.prj
        reg_France_DOM.qpj
        reg_France_DOM.shp
        reg_France_DOM.shx
```

## Exemples d'utilisation 



```r
library(sf)
library(ggplot2)
```

Lecture des fonds de carte avec {sf} :


```r
fonds_dept <- st_read("fonds_detailles/dep_France_DOM_zoomPC.shp")
#> Reading layer `dep_France_DOM_zoomPC' from data source `C:\Users\AQEW8W\Downloads\fonds_carto_m-tro_dom\fonds_detailles\dep_France_DOM_zoomPC.shp' using driver `ESRI Shapefile'
#> Simple feature collection with 105 features and 3 fields
#> geometry type:  MULTIPOLYGON
#> dimension:      XY
#> bbox:           xmin: 99225.97 ymin: 6049647 xmax: 1082671 ymax: 7110480
#> epsg (SRID):    NA
#> proj4string:    +proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +units=m +no_defs
fonds_reg <- st_read("fonds_detailles/reg_France_DOM.shp")
#> Reading layer `reg_France_DOM' from data source `C:\Users\AQEW8W\Downloads\fonds_carto_m-tro_dom\fonds_detailles\reg_France_DOM.shp' using driver `ESRI Shapefile'
#> Simple feature collection with 18 features and 2 fields
#> geometry type:  MULTIPOLYGON
#> dimension:      XY
#> bbox:           xmin: 99225.97 ymin: 6049647 xmax: 1082671 ymax: 7110480
#> epsg (SRID):    NA
#> proj4string:    +proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +units=m +no_defs
```

Affichage du fonds de carte départemental :  


```r
ggplot(fonds_dept) +
  geom_sf() +
  aes(fill = reg) +
  coord_sf(crs = 4326) +
  guides(fill = FALSE) +
  ggtitle("Ma carte par département")
```

<img src="man/figures/README-unnamed-chunk-4-1.png" width="100%" />

